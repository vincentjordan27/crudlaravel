@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <a href="/pertanyaan/create" class="btn btn-primary mb-1">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">
                <th scope="col">Title</th>
                <th scope="col">Body</th>
                <th scope="col" style="width: 20%">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($post as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->isi}}</td>
                        <td style="display: flex; justify-content: space-between;">
                            <a href="/pertanyaan/{{$value->id}}" class="btn btn-info m-1">Show</a>
                            <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary m-1">Edit</a>
                            <form action="/pertanyaan/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger m-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td style="text-align: center" colspan="4">No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>
</div>
@endsection